doing_good_for_kaizen = {
    cash_reserves_ratio > 0.8
    weekly_profit > 1000
    occupancy >= 0.6
    NOT = {
        has_modifier = kaizen_improvement_cooldown
    }
}

doing_bad_for_kaizen = {
    cash_reserves_ratio > 0.1
    weekly_profit < 5000
    NOT = {
        has_modifier = kaizen_improvement_cooldown
    }
}

no_kaizen_modifier = {
    NOR = {
        has_modifier = kaizen_improvement_1
        has_modifier = kaizen_improvement_2
        has_modifier = kaizen_improvement_3
        has_modifier = kaizen_improvement_4
        has_modifier = kaizen_improvement_5
        has_modifier = kaizen_improvement_6
        has_modifier = kaizen_improvement_7
        has_modifier = kaizen_improvement_8
        has_modifier = kaizen_improvement_9
        has_modifier = kaizen_improvement_10
        has_modifier = kaizen_improvement_11
        has_modifier = kaizen_improvement_12
        has_modifier = kaizen_improvement_13
        has_modifier = kaizen_improvement_14
        has_modifier = kaizen_improvement_15
        has_modifier = kaizen_improvement_16
        has_modifier = kaizen_improvement_17
        has_modifier = kaizen_improvement_18
        has_modifier = kaizen_improvement_19
        has_modifier = kaizen_improvement_20
        has_modifier = kaizen_improvement_21
        has_modifier = kaizen_improvement_22
        has_modifier = kaizen_improvement_23
        has_modifier = kaizen_improvement_24
        has_modifier = kaizen_improvement_25
        has_modifier = kaizen_improvement_26
        has_modifier = kaizen_improvement_27
        has_modifier = kaizen_improvement_28
        has_modifier = kaizen_improvement_29
        has_modifier = kaizen_improvement_30
        has_modifier = kaizen_improvement_31
        has_modifier = kaizen_improvement_32
        has_modifier = kaizen_improvement_33
        has_modifier = kaizen_improvement_34
        has_modifier = kaizen_improvement_35
        has_modifier = kaizen_improvement_36
        has_modifier = kaizen_improvement_37
        has_modifier = kaizen_improvement_38
        has_modifier = kaizen_improvement_39
        has_modifier = kaizen_improvement_40
        has_modifier = kaizen_improvement_41
        has_modifier = kaizen_improvement_42
        has_modifier = kaizen_improvement_43
        has_modifier = kaizen_improvement_44
        has_modifier = kaizen_improvement_45
        has_modifier = kaizen_improvement_46
        has_modifier = kaizen_improvement_47
        has_modifier = kaizen_improvement_48
        has_modifier = kaizen_improvement_49
        has_modifier = kaizen_improvement_50
    }
}