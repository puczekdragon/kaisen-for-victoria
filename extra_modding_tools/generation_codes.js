//those are JS codes to generate long chains of mod lines automatically, to use them, paste on of them into the console in browser and copy output

let j, output;
for(let i = 1 ; i < 51 ; i++) {
    if (i < 10) j = '0' + i;
    else j = i;
    output = output + `
kaizen_improvement_${i} = {
    icon = gfx/interface/icons/timed_modifier_icons/modifier_lightbulb_positive.dds
    building_throughput_mult = 0.${j}
}`;
}
console.log(output);


let j, output;
for(let i = 1 ; i < 51 ; i++) {
    if (i < 10) j = '0' + i;
    else j = i;
    output = output + `
has_modifier = kaizen_improvement_${i}`;
}
console.log(output);

let j, output;
for(let i = 1 ; i < 51 ; i++) {
    j = i+1;
    output = output + `
else_if = {
    limit = { 
        has_modifier = kaizen_improvement_${i}
    }	
    remove_modifier = kaizen_improvement_${i}
    add_modifier = {
        name = kaizen_improvement_${j}
        months = -1
    }
}`;
}
console.log(output);

let j, output;
for(let i = 1 ; i < 51 ; i++) {
    j = i+1;
    output = output + `
else_if = {
    limit = { 
        has_modifier = kaizen_improvement_${j}
    }	
    remove_modifier = kaizen_improvement_${j}
    add_modifier = {
        name = kaizen_improvement_${i}
        months = -1
    }
}`;
}
console.log(output);


let j, output;
for(let i = 1 ; i < 51 ; i++) {
    if (i < 10) j = '0' + i;
    else j = i;
    output = output + `
kaizen_improvement_${i}:0 "Kaizen!"`;
}
console.log(output);